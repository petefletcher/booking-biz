import "./componentscss/appointmentsCalendar.css";
import dateFns from "date-fns";
import DayView from "./DayView";
import { useState } from 'react'; 
import React from 'react';
import {useDispatch} from 'react-redux';

const Calendar = () => {
    const [currentMonth, setCurrentMonth] = useState(new Date())
    const [selectedDate, setselectedDate] = useState("")
    const [currentDay, setcurrentDay] = useState(dateFns.startOfToday())
    const [toggleView, settoggleView] = useState({view: "Month"})

    const dispatch = useDispatch();
    dispatch({ 
      type: 'day-header', payload: currentDay
    })

  const renderMonthHeader = () => {
    return (
      <div className="skip-button-wrap">
      <div className="skip-button" onClick={prevMonth}>
              Prev
            </div>
            <div className="skip-button" onClick={nextMonth}>
              Next
      </div>
      </div>   
    );
  }

  const renderDayHeader = () => {
    return (
      <div className="skip-button-wrap">
      <div className="skip-button" onClick={prevDay}>
            Prev
          </div>
          <div className="skip-button" onClick={nextDay}>
            Next
          </div>
      </div>
    )
}

  const renderHeader = () => {
    let dateFormat;
    let dayFormat;

    if(window.screen.width  < 411  && window.screen.width > 320) {
      dayFormat = "ddd DD MMM YY";
      dateFormat = "MMM YYYY";
    } else if(window.screen.width  <= 320 ) {
      dateFormat = "MMM YYYY";
      dayFormat = "ddd DD MMM";
    } else {
      dateFormat = "MMM YYYY";
      dayFormat = "ddd DD MMM";
    }

    return (
      <div className="month-wrapper-main">
        <div className="header-wrap">
          {toggleView.view === "Month" ? renderMonthHeader() : renderDayHeader()}

          {toggleView.view === "Month" ? <header className="month-header">{dateFns.format(currentMonth, dateFormat)}</header> : <header className="day-header">{dateFns.format(currentDay, dayFormat)}</header>}
          <div>
            <select className="selector" onChange={(e) => selectOnChange(e)} value={toggleView.view}>
              <option className="selection" value="Day">Day</option>
              <option className="selection" value="Month">Month</option>
            </select>
          </div>
        </div>
      </div>
    );
  }

  const renderDays = () => {
    const dateFormat = "ddd";
    const days = [];
    let startDate = dateFns.startOfWeek(currentMonth);

    for (let i = 0; i < 7; i++) {
      days.push(
        <p className="col col-center" key={i}>
          {dateFns.format(dateFns.addDays(startDate, i), dateFormat)}
        </p>
      );
    }
    return <div className="day-names">{days}</div>;
  }

  const selectOnChange = (event) => {
    const newView = event.target.value
    settoggleView({
      view: newView
    })
  }

  const renderCells = () => {
 
    const monthStart = dateFns.startOfMonth(currentMonth);
    const monthEnd = dateFns.endOfMonth(monthStart);
    const startDate = dateFns.startOfWeek(monthStart);
    const endDate = dateFns.endOfWeek(monthEnd);
    
    const dateFormat = "D";

    let days = [];
    let day = startDate;
    let formattedDate = "";

    while (day <= endDate) {
      for (let i = 0; i < 7; i++) {
        formattedDate = dateFns.format(day, dateFormat);
        const cloneDay = day.toString();
        const today = dateFns.startOfToday();
        let colour = '';
        let textColor = '';
        
        if (today.toString() === cloneDay) {
          textColor = '#fff'
          colour = '#FF7000'
        }

        days.push(
          <div
            className="day"
            key={day}
            style={{ 'backgroundColor': colour.toString() }}
            onClick={() => onDateClick(dateFns.parse(cloneDay))}
          >
            <span className="mini-day-number" style={{ 'color': textColor.toString()}}>{formattedDate}</span>
          </div>
        );
        day = dateFns.addDays(day, 1);
      }
    }
    return <div className="days">{days}</div>;
  }

  const renderDayView = () => {
    return <DayView day={selectedDate}/>
  }

  const nextMonth = () => {
    setCurrentMonth(dateFns.addMonths(currentMonth, 1))
  };

  const prevMonth = () => {
    setCurrentMonth(dateFns.subMonths(currentMonth, 1))
  };

  const onDateClick = day => {
    const clickDate = day
    const toggle = "Day"
    setselectedDate(clickDate)
    settoggleView({
      view: toggle
    })
    setcurrentDay(clickDate)
    return <DayView day={clickDate}/>
  };

  const nextDay = () => {
    setcurrentDay(dateFns.addDays(currentDay, 1))
  };

  const prevDay = () => {
    setcurrentDay(dateFns.subDays(currentDay, 1))
  };
    return ( 
        <div className="month-wrapper-full">
        {renderHeader()}
          <div className="month-days-wrapper">
            {toggleView.view === "Month" ? renderDays() : null}
            {toggleView.view === "Month" ? renderCells() : null}
            {toggleView.view === "Day" ? renderDayView(selectedDate) : null}
          </div>
        </div>
    );
  }

export default Calendar;
