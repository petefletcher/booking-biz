import React from "react";
import Calendar from "./MyCalendar.js";
import {useSelector} from 'react-redux';
import './componentscss/Appointments.css';

const Appointments = () => { 
  const sideMenuToggle = useSelector(state => state.uiFunctionality.sideMenuToggle)
  
  let left = '';
  let width = '';
  if(sideMenuToggle) {
    left = '14%'
    width = '84%'
  } else {
    left = '';
    width = '100%';
  }
    return (
      <React.Fragment>
        <section className="appointment-calendar" style={{left: left, width: width}}><Calendar/></section> </React.Fragment>
    );
};

export default Appointments;
