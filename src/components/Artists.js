import React from "react";
import { useSelector } from "react-redux";
import { navigate } from "@reach/router";
import "./componentscss/Artist.css";
import Avatar from "../assets/captain.png";

import BgImage from "../assets/vida-loca-tattoo-shop-front.jpg";

const Artists = props => {
  const storeEmployees = useSelector(state => state.employeesdb.data);

  return (
    <section className="background-artists">
      <img src={BgImage} className="background-image" alt="studio" />
      <div className="main">
        <div className="artist-header">
          <p>Artists</p>
        </div>
        <section className="artist-container">
          {storeEmployees.map((employee, index) => {
            return (
              <div
                onClick={() =>
                  navigate(`/artists/${employee.email}`, {
                    state: { storeEmployees: employee }
                  })
                }
                className="artist-page-employee"
                key={`${employee.employeeFirstName}-${index}`}
              >
                <img src={Avatar} />
                <div className="artist-page--employee-name">
                  {`${employee.employeeFirstName} ${employee.employeeLastName}`}
                </div>
              </div>
            );
          })}
        </section>
      </div>
    </section>
  );
};

export default Artists;
