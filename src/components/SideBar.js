import React, { useState, useEffect } from "react";
import { Link } from "@reach/router";
import "./componentscss/Sidebar.css";
import { useSelector, useDispatch } from "react-redux";
import { updateView, setAuthenticated } from "../store/actions";
import axios from "axios";

const Sidebar = () => {
  const storeEmployees = useSelector(state => state.employeesdb.employees);
  const appointmentView = useSelector(
    state => state.employeesdb.viewAppointments
  );
  const dispatch = useDispatch();

  const [artistList, setArtistList] = useState(storeEmployees);
  const [laserList, setLaserList] = useState([]);
  const [piercingList, setPiercingList] = useState([]);

  useEffect(() => {
    const tattooArtists = [];
    artistList.forEach(element => {
      if (element.service === "tattoo") tattooArtists.push(element.name);
    });
    setArtistList(tattooArtists);
    return () => {
      console.log("Clean-up-tattoists");
    };
  }, []);

  useEffect(() => {
    const laserArtist = [];
    artistList.forEach(element => {
      if (element.service === "laser") laserArtist.push(element.name);
    });
    setLaserList(laserArtist);
    return () => {
      console.log("Clean-up-laser");
    };
  }, []);

  useEffect(() => {
    const piercingArtists = [];
    artistList.forEach(element => {
      if (element.service === "piercings") piercingArtists.push(element.name);
    });
    setPiercingList(piercingArtists);
    return () => {
      console.log("Clean-up-piercings");
    };
  }, []);

  const [appointmentClick, setAppointmentClick] = useState(false);
  const [artistClick, setArtistClick] = useState(false);
  const [laserClick, setLaserClick] = useState(false);
  const [piercingClick, setPiercingClick] = useState(false);

  const handleArtistFilter = artist => {
    dispatch({
      type: updateView,
      payload: artist
    });
  };

  const handleAppointment = appointmentClick => {
    const noShow = appointmentClick;
    setAppointmentClick(!noShow);
  };

  const handleArtistClick = artistClick => {
    console.log("Artist Click");
    dispatch({
      type: updateView,
      payload: "All"
    });
    const noShow = artistClick;
    setArtistClick(!noShow);
  };

  const handleLaserClick = laserClick => {
    dispatch({
      type: updateView,
      payload: "All"
    });
    const noShow = laserClick;
    setLaserClick(!noShow);
  };

  const handlePiercingClick = piercingClick => {
    dispatch({
      type: updateView,
      payload: "All"
    });
    const noShow = piercingClick;
    setPiercingClick(!noShow);
  };

  const handleLogout = () => {
    localStorage.removeItem("firebaseToken");
    delete axios.defaults.headers.common["Authorization"];
    const unauthenticated = false;
    dispatch({ type: setAuthenticated, payload: unauthenticated });
    window.location.href = "/";
  };

  console.log("Artist list", artistList);
  return (
    <section className="sidebar">
      <button>
        <Link to="dashboard">Dashboard</Link>
      </button>
      <button onClick={() => handleAppointment(appointmentClick)}>
        <Link to="appointments">Appointments</Link>
      </button>
      {appointmentClick ? (
        <div className="dropdown-container">
          <button onClick={() => handleArtistClick(artistClick)}>
            Tattoo Appointments
          </button>
          {artistClick
            ? artistList.map(artist => {
                return (
                  <div className="dropdown-artist" key={artist + "-div"}>
                    <button
                      key={artist}
                      className="artist-button"
                      onClick={() => handleArtistFilter(artist)}
                    >
                      {artist}
                    </button>
                  </div>
                );
              })
            : null}

          <button onClick={() => handleLaserClick(laserClick)}>
            Laser Appointments
          </button>
          {laserClick
            ? laserList.map(artist => {
                return (
                  <div className="dropdown-artist">
                    <button id={artist} className="artist-button">
                      {artist}
                    </button>
                  </div>
                );
              })
            : null}
          <button onClick={() => handlePiercingClick(piercingClick)}>
            Piercings & Cosmetics
          </button>
          {piercingClick
            ? piercingList.map((artist, index) => {
                return (
                  <div className="dropdown-artist">
                    <button id={artist} className="artist-button">
                      {artist}
                    </button>
                  </div>
                );
              })
            : null}
        </div>
      ) : null}
      <button onClick={() => handleArtistClick(artistClick)}>
        <Link to="artists">Artists</Link>
      </button>

      <button>
        <Link to="clients">Clients</Link>
      </button>
      <button>
        <Link to="admin">Admin</Link>
      </button>
      <button>
        <Link to="/signup">Sign Up</Link>
      </button>
      <div>
        <button className="btn btn-primary" onClick={() => handleLogout()}>
          Logout
        </button>
      </div>
    </section>
  );
};

export default Sidebar;
