import React, { useState} from "react";
import { Link } from "@reach/router";
import {useSelector, useDispatch} from 'react-redux';
import "./componentscss/Navbar.css";
import Logo from '../assets/Vida-Loca-Tattoo-Bolton-Logo-BW.png'
import Avatar from '../assets/batman-vidaloca.png';

const Navbar = () => {
  const sideMenuToggle = useSelector(state => state.uiFunctionality.sideMenuToggle)
  const [toggleSideMenu, setSideMenu] = useState(sideMenuToggle)
  const dispatch = useDispatch()

  const handleSideMenu = () => {
    const doesShow = !toggleSideMenu;
    setSideMenu(doesShow);
    dispatch({
      type: 'toggle-sidebar', payload: doesShow
    })
  }


  return (
    <React.Fragment>
      <nav className="topnav">

          <div className="company-logo">
            <img src={Logo} onClick={handleSideMenu} alt="vida-tattoo-logo"/>
          </div>


          <div className="company-name">
            <p></p>
          </div>


        <div className="nav-user">
        <Link to="appointments"><img src={Avatar} alt="vida-tattoo-logo"/></Link>
        </div>

      </nav>
      </React.Fragment>
    );
}

export default Navbar;
