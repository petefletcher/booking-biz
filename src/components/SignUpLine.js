import React from "react";
import {Link} from '@reach/router';
import './componentscss/Signupline.css';
import { useDispatch} from 'react-redux';
import {setLogin} from '../store/actions'

const SignUpLine = () => {

const dispatch = useDispatch()

const handleClick = () => {
    const loginScreen = false 
    dispatch({
        type: setLogin,
        payload: loginScreen
    })
}
    return ( 
        <section className="signup-line">
            <small>
                Dont have an account ? sign up <Link to="/signup" onClick={handleClick}>here</Link>
            </small>
        </section>
     ) ;
}
 
export default SignUpLine;