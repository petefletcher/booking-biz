import React, { useState, useImperativeHandle } from "react";
import BgImage from "../assets/vida-loca-tattoo-shop-front.jpg";
import "./componentscss/ArtistPage.css";
import Avatar from "../assets/dan-watson-card.jpg";
import DayView from "../components/DayView";
import { useSelector, useDispatch } from "react-redux";
import { updateView } from "../store/actions";
import dateFns from "date-fns";

const ArtistPage = props => {
  const storeEvents = useSelector(state => state.eventsFile.events);
  const appointmentView = useSelector(
    state => state.eventsFile.viewAppointments
  );
  const dispatch = useDispatch();

  const [tabs, setTabs] = useState({
    general: true,
    customers: false,
    calendar: false,
    hours: false,
    appointments: false
  });

  const {
    email,
    employeeFirstName,
    employeeLastName,
    imageURL,
    mobile,
    service
  } = props.location.state.storeEmployees;
  const backgroundColor = "background-color";

  const handleTabClick = event => {
    const { className } = event.target;
    const tab = className.split("-").pop();
    setTabs({
      general: false,
      customers: false,
      calendar: false,
      hours: false,
      upcoming: false,
      [tab]: true
    });
  };

  const renderMiniCal = () => {
    dispatch({
      type: updateView,
      payload: employeeFirstName
    });
    return (
      <section className="artist-page--calendar-wrapper">
        <DayView />
      </section>
    );
  };

  const renderCustomers = () => {
    let customersArray = ["Pete Pepper", "Fredrick Salt", "John Smith"];
    storeEvents.filter(client => {
      if (client.employeeId === employeeFirstName) {
        customersArray.push(client.client);
      }
    });
    return (
      <section className="artist-page--customer-wrapper">
        {customersArray.map((customer, index) => {
          return (
            <div className={`artist-client-${customer}`} key={index}>
              {customer}
            </div>
          );
        })}
      </section>
    );
  };

  const renderProfileDetails = () => {
    return (
      <div className="profile-details">
        <div className="emp-service" onClick={() => console.log("service")}>
          {service.charAt(0).toUpperCase() + service.substring(1)}
        </div>
        <div className="emp-mobile" onClick={() => console.log("mobile")}>
          07914875223
        </div>
        <div className="emp-email" onClick={() => console.log("email")}>
          {email}
        </div>
        <div className="emp-message" onClick={() => console.log("message")}>
          Message Employee
        </div>
        <div className="emp-edit" onClick={() => console.log("edit")}>
          Edit Employee
        </div>
      </div>
    );
  };

  const renderHours = () => {
    let hoursThisWeek = [];
    let today = dateFns.startOfToday();
    var weekBeginning = dateFns.startOfWeek(today, {
      weekStartsOn: 1
    });
    var weekEnd = dateFns.endOfWeek(today, {
      weekStartsOn: 1
    });

    weekBeginning = dateFns.format(weekBeginning, "ddd MMM DD YYYY");
    weekEnd = dateFns.format(weekEnd, "ddd MMM DD YYYY");

    storeEvents.forEach(event => {
      if (event.employeeId == employeeFirstName) {
        if (
          dateFns.isBefore(event.date, weekBeginning) ||
          dateFns.isAfter(event.date, weekEnd)
        ) {
        } else {
          hoursThisWeek.push(event);
        }
      }
    });

    let totalFinishId = hoursThisWeek.reduce((acc, val) => {
      return acc + val.finishTimeId;
    }, 0);
    let totalStartId = hoursThisWeek.reduce((acc, val) => {
      return acc + val.timeId;
    }, 0);

    let totalWeeksHours = (totalFinishId - totalStartId) / 2;

    return (
      <section className="artist-page--hours-wrapper">
        {hoursThisWeek.map((event, index) => {
          return (
            <div className={`artist-hours-${event}`} key={index}>
              <div>{event.date}</div>
              <div>
                Start {event.startTime} -- Finish :{event.endTime}
              </div>
            </div>
          );
        })}
        <div className="artist-page--total-hours">
          <h5>Total Hours This Week {totalWeeksHours}</h5>
        </div>
      </section>
    );
  };

  const renderUpcomingBookings = () => {
    let upcomingEvents = [];
    let today = dateFns.startOfToday();
    let nextThirty = dateFns.addDays(today, 30);
    let yday = dateFns.addDays(today, -1);

    storeEvents.forEach(event => {
      if (
        event.employeeId == employeeFirstName &&
        dateFns.isAfter(event.date, yday) &&
        dateFns.isBefore(event.date, nextThirty)
      ) {
        upcomingEvents.push(event);
      }
    });
    return (
      <section className="artist-page--upcoming-wrapper">
        {upcomingEvents.map((event, index) => {
          return (
            <div className={`artist-upcoming-${event}`} key={index}>
              <div>{event.date}</div>
              <div>
                Start {event.startTime} -- Finish :{event.endTime}
              </div>
            </div>
          );
        })}
      </section>
    );
  };

  return (
    <section className="background-artist-page">
      <img src={BgImage} className="background-image" alt="studio" />
      <div className="main">
        <div className="artist-page--profile-header">
          <p>Artist Profile</p>
        </div>

        <section className="artist-profile-wrapper">
          <section className="profile-tabs">
            <div className="spacer">
              <div className="artist-page-name">Dan Watson</div>
            </div>
            <section className="artist-page-tab-section">
              {tabs.general ? (
                <div
                  className="tab-general"
                  style={{ backgroundColor: "#df6100", color: "#fff" }}
                  onClick={event => handleTabClick(event)}
                >
                  General
                </div>
              ) : (
                <div
                  className="tab-general"
                  onClick={event => handleTabClick(event)}
                >
                  General
                </div>
              )}
              {tabs.customers ? (
                <div
                  className="tab-customers"
                  style={{ backgroundColor: "#df6100", color: "#fff" }}
                  onClick={event => handleTabClick(event)}
                >
                  Customers
                </div>
              ) : (
                <div
                  className="tab-customers"
                  onClick={event => handleTabClick(event)}
                >
                  Customers
                </div>
              )}
              {tabs.calendar ? (
                <div
                  className="tab-qv-calendar"
                  style={{ backgroundColor: "#df6100", color: "#fff" }}
                  onClick={event => handleTabClick(event)}
                >
                  Calendar
                </div>
              ) : (
                <div
                  className="tab-qv-calendar"
                  onClick={event => handleTabClick(event)}
                >
                  Calendar
                </div>
              )}
              {tabs.hours ? (
                <div
                  className="tab-qv-hours"
                  style={{ backgroundColor: "#df6100", color: "#fff" }}
                  onClick={event => handleTabClick(event)}
                >
                  Hours
                </div>
              ) : (
                <div
                  className="tab-qv-hours"
                  onClick={event => handleTabClick(event)}
                >
                  Hours
                </div>
              )}
              {tabs.appointments ? (
                <div
                  className="tab-next-appointments"
                  style={{ backgroundColor: "#df6100", color: "#fff" }}
                  onClick={event => handleTabClick(event)}
                >
                  Upcoming Bookings
                </div>
              ) : (
                <div
                  className="tab-next-appointments"
                  onClick={event => handleTabClick(event)}
                >
                  Upcoming Bookings
                </div>
              )}
            </section>
          </section>
          <section className="profile-content">
            <div className="profile-image">
              <div>
                <img
                  src={Avatar}
                  className={`user-image-${employeeFirstName}-${employeeLastName}`}
                  id={`user-image-${employeeFirstName}-${employeeLastName}`}
                />
              </div>
            </div>
            {tabs.general ? renderProfileDetails() : null}
            {tabs.calendar ? renderMiniCal() : null}
            {tabs.customers ? renderCustomers() : null}
            {tabs.hours ? renderHours() : null}
            {tabs.appointments ? renderUpcomingBookings() : null}
          </section>
        </section>
      </div>
    </section>
  );
};

export default ArtistPage;
