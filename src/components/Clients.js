import React, { useState } from "react";

const style = {
  top: '40%',
  left: '40%',
  position: 'fixed'
};
const Clients = props => {
    const [clicked, setClicked] = useState(false)

    const handleClick = (clicked) => {
      // const noShow = clicked
      setClicked(!clicked)
    }


    return (
    <button style={style} onClick={() => handleClick(clicked)}>
      <div>Clients</div>
      {clicked ? <h1>Clicked</h1> : null}
      </button>
    )
}

export default Clients;