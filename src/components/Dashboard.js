import React from "react";
import ArtistCard from "./DashCards/ArtistStatusCard.js";
import ClientCard from "./DashCards/ClientStatusCards.js";
import BookingsCard from "./DashCards/NewBookings.js";
import CancellationCard from "./DashCards/CancellationCard.js";
import "./componentscss/Dashboard.css";
import AppointmentCard from "./ReminderCards/AppointmentsCard";
import LaserCard from "./ReminderCards/LaserCards";
import PiercingCard from "./ReminderCards/PiercingCards";
import FreeSlotsCard from "./ReminderCards/FreeSlotsCard";
import MiniCal from "../components/miniCal";
import BgImage from "../assets/vida-loca-tattoo-shop-front.jpg";

const Dashboard = () => {
  return (
    <section className="background-dash">
      <img src={BgImage} alt="studio" />
      <div className="main">
        <div className="dash-header">
          <p>Dashboard</p>
        </div>

        <div className="main-overview-dashboard">
          <div className="overviewcard">
            <ArtistCard />
          </div>
          <div className="overviewcard">
            <ClientCard />
          </div>
          <div className="overviewcard">
            <BookingsCard />
          </div>
          <div className="overviewcard">
            <CancellationCard />
          </div>
        </div>

        <div className="cards-calendar">
          <div className="main-cards">
            <div className="card">
              <AppointmentCard />
            </div>
            <div className="card">
              <LaserCard />
            </div>
            <div className="card">
              <PiercingCard />
            </div>
            <div className="card">
              <FreeSlotsCard />
            </div>
          </div>

          <div className="calendar-section">
            <div className="calendar">
              <MiniCal />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Dashboard;
