import "./componentscss/MiniCal.css";
import dateFns from "date-fns";
import { useState} from 'react'; 
import React from 'react';
import { navigate } from '@reach/router';


const MiniCal = () => {
    const [currentMonth, setCurrentMonth] = useState(new Date())
    const currentDay = dateFns.startOfToday();
    const toggleView = {view: "Month"};

  const renderMonthHeader = () => {
    return (
      <div className="skip-button-wrap-mini">
      <div className="skip-button-mini" onClick={prevMonth}>
              Prev
            </div>
            <div className="skip-button-mini" onClick={nextMonth}>
              Next
      </div>
      </div>   
    );
  }

  const renderHeader = () => {
    let dayFormat; 

    if(window.screen.width  < 411 ) {
      dayFormat = "ddd DD MMM YYY";
    }
    const dateFormat = "MMM YYYY";

    return (

        <div className="mini-header-wrap">
          {renderMonthHeader()}
          <div className="month-text-wrapper">
          {toggleView.view === "Month" ? <header className="month-header" >{dateFns.format(currentMonth, dateFormat)}</header> : <header className="day-header">{dateFns.format(currentDay, dayFormat)}</header>}
          </div>
          
          <div className="mini-selector">
            </div>

        </div>
  
    );
  }

  const renderDays = () => {
    const dateFormat = "ddd";
    const days = [];
    let startDate = dateFns.startOfWeek(currentMonth);

    for (let i = 0; i < 7; i++) {
      days.push(
        <p className="col col-center" key={i}>
          {dateFns.format(dateFns.addDays(startDate, i), dateFormat)}
        </p>
      );
    }
    return <div className="mini-day-names">{days}</div>;
  }
  
    const toAppointments = () => {
      return navigate('/appointments')
    }
   const renderCells = () => {
 
    const monthStart = dateFns.startOfMonth(currentMonth);
    const monthEnd = dateFns.endOfMonth(monthStart);
    const startDate = dateFns.startOfWeek(monthStart);
    const endDate = dateFns.endOfWeek(monthEnd);
    
    const dateFormat = "D";

    let days = [];
    let day = startDate;
    let formattedDate = "";

    while (day <= endDate) {
      for (let i = 0; i < 7; i++) {
        formattedDate = dateFns.format(day, dateFormat);

        const cloneDay = day.toString();
        const today = dateFns.startOfToday();
        let colour = '';
        let textColor = '';
          
        if (today.toString() === cloneDay) {
          colour = '#df6100'
          textColor = '#fff'
        }

         days.push(
          <div
            className="mini-day"
            key={day}
            style={{ 'backgroundColor': colour.toString()}}
             >
            <span className="mini-day-number" style={{ 'color': textColor.toString()}}>{formattedDate}</span>
          </div>
        );
        day = dateFns.addDays(day, 1);
      }
    }
    return <div className="mini-days" onClick={toAppointments}  >{days}</div>;
  }


  const nextMonth = () => {
    setCurrentMonth(dateFns.addMonths(currentMonth, 1))
  };

  const prevMonth = () => {
    setCurrentMonth(dateFns.subMonths(currentMonth, 1))
  };

  // const nextDay = () => {
  //   setcurrentDay(dateFns.addDays(currentDay, 1))
  // };

  // const prevDay = () => {
  //   setcurrentDay(dateFns.subDays(currentDay, 1))
  // };
    return ( 
        <div className="mini-month-wrapper">
        {renderHeader()}
          <div className="mini-month-days-wrapper">
            {toggleView.view === "Month" ? renderDays() : null}
            {toggleView.view === "Month" ? renderCells() : null}
            </div>
        </div>
    );
  }

export default MiniCal;
