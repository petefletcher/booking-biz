import React from "react";
import times from "../utils/times.json";
import "./componentscss/EditEventModal.css";

class EditEventModal extends React.Component {
  state = {
    appointmentData: {
      employeeId: this.props.event.employeeId,
      startTime: this.props.event.startTime,
      endTime: this.props.event.endTime,
      client: this.props.event.client,
      details: this.props.event.details,
      timeId: this.props.event.timeId,
      finishTimeId: this.props.event.finishTimeId,
      date: this.props.event.date
    }
  };
  render() {

    return (
      <div className="appointment-modal-content">
        <div className="appointment-modal-header">
          <h5 className="appointment-modal-title" id="editModal">
            Edit Appointment
          </h5>
        </div>
        <div className="appointment-modal-body">
          <form id="appointmentData" onSubmit={this.handleSubmit}>
            <div className="appointment-form-group">
              <label>Date</label>
              <input
                type="text"
                className="appointment-form-control"
                id="date"
                value={this.state.appointmentData.date}
                disabled
              />
            </div>
            <div className="appointment-form-group">
              <label>Booking With</label>
              <input
                type="text"
                className="appointment-form-control"
                id="date"
                value={this.state.appointmentData.employeeId}
                disabled
              />
            </div>
            <div className="appointment-form-group">
              <label>Start Time</label>
              <select
                className="appointment-form-control"
                id="startTime"
                onChange={this.selectOnChange}
                value={this.state.appointmentData.startTime}
              >
                {this.renderStartTimes()}
              </select>
            </div>
            <div className="appointment-form-group">
              <label>End Time</label>
              <select
                className="appointment-form-control"
                id="endTime"
                onChange={this.selectOnChange}
                value={this.state.appointmentData.endTime}
              >
                {this.renderFinishTimes()}
              </select>
            </div>
          </form>
          <div className="appointment-form-group">
            <label>Client Name</label>
            <input
              type="text"
              className="appointment-form-control"
              id="client"
              onChange={this.selectOnChange}
              value={this.state.appointmentData.client}
            />
          </div>
          <div className="appointment-form-group">
            <label>Details</label>
            <textarea
              className="appointment-form-control"
              id="details"
              rows="3"
              onChange={this.selectOnChange}
              value={this.state.appointmentData.details}
            />
          </div>
        </div>
        <div className="appointment-modal-footer">
          <button
            type="button"
            className="close-button"
            data-dismiss="modal"
            onClick={this.props.onHide}
          >
            Close
          </button>
          <button
            type="button"
            className="save-button"
            onClick={event => {
              this.selectOnChange(event);
              this.props.onHide();
              this.props.saveEditedAppointment(this.state.appointmentData);
            }}
          >
            Save changes
          </button>
          <button
            type="button"
            className="delete-button"
            onClick={event => {
              this.selectOnChange(event);
              this.props.onHide();
              this.props.deleteAppointment(this.state.appointmentData);
            }}
          >
            Delete
          </button>
        </div>
      </div>
    );
  }
    renderStartTimes = function() {
    let aptTimes = [];
        
    for (let i = 0; i < times.length; i++) {
            aptTimes.push(
              <option value={times[i]} id={'startTimeId' + i}>{times[i]}</option>
            );
    }

    return aptTimes;
  };
  renderFinishTimes = () => {
    let aptTimes = [];

    for (let i = 0; i < times.length; i++) {
      aptTimes.push(
        <option value={times[i]} id={'finishTimeId' + i}>{times[i]}</option>
      );
    }
    return aptTimes;
  };

  
  selectOnChange = event => {
    const { id, value } = event.target;
    let startId = this.state.appointmentData.timeId;
    if (id === 'startTime') {
        startId = event.target.selectedIndex
    }
    let finishId = this.state.appointmentData.finishTimeId;
    if (id === 'endTime') {
        finishId = event.target.selectedIndex
    }
    this.setState({
      appointmentData: {
        ...this.state.appointmentData,
        timeId: startId,
        finishTimeId: finishId,
        [id]: value
      }
    });
  };
}

export default EditEventModal;
