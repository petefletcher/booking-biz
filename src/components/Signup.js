import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { useState } from "react";
import axios from "axios";
import "./componentscss/Signup.css";
import { Link, navigate } from "@reach/router";
import {
  setErrors,
  clearErrors,
  loadingUi,
  setUser,
  setLogin
} from "../store/actions";
import {getUserData} from '../utils/axiosCalls';


const SignUp = () => {
  const superrors = useSelector(state => state.uiFunctionality.errors);

  const [signUpErrors, setError] = useState({
    email: "",
    password: "",
    confirmPassword: "",
    firstName: "",
    lastName: "",
    mobile: ""
  });
  const [signUpDetails, setSignUpDetails] = useState({
    email: "",
    password: "",
    confirmPassword: "",
    firstName: "",
    lastName: "",
    mobile: ""
  });
  const dispatch = useDispatch();

  const signUpPost = () => {
    axios
      .post(`http://localhost:5000/api/signup`, signUpDetails)
      .then(result => {
        localStorage.setItem("firebaseToken", `Bearer ${result.data.token}`);
        dispatch({ type: clearErrors });
        const loginScreen = true;
        dispatch({
          type: setLogin,
          payload: loginScreen
      })
        navigate(`/dashboard`);
      }).then(() => {
        const loginScreen = true;
        dispatch({
          type: setLogin,
          payload: loginScreen
      })
        navigate(`/dashboard`);
      })
      .catch(err => {
        dispatch({
          type: setErrors,
          payload: err.response.data
        });
      });
  };

  const validate = data => {
    //.*matches any char
    //\d any digit
    //{4,15} four to 15 chars
    const passwordCheck = new RegExp("^(?=.*d){4,15}$");

    if (data.email === "") {
      setError({
        ...signUpErrors,
        email: "Email must not be empty"
      });
      return false;
    }

    if (data.password === "") {
      setError({
        ...signUpErrors,
        password: "Password must not be empty"
      });
      return false;
    }

    if (data.confirmPassword === "") {
      setError({
        ...signUpErrors,
        confirmPassword: "Password must not be empty"
      });
      return false;
    }

    if (data.firstName === "") {
      setError({
        ...signUpErrors,
        firstName: "firstName must not be empty"
      });
      return false;
    }

    if (data.lastName === "") {
      setError({
        ...signUpErrors,
        lastName: "lastName must not be empty"
      });
      return false;
    }

    if (data.mobile === "") {
      setError({
        ...signUpErrors,
        mobile: "Mobile must not be empty"
      });
      return false;
    }

    if (!data.email.includes("@")) {
      setError({
        ...signUpErrors,
        email: "Email not valid"
      });
      return false;
    }

    if (passwordCheck.test(data.password)) {
      setError({
        ...signUpErrors,
        password:
          "Passwords must must be be 4 chars long and include at least one numeric digit"
      });
      return false;
    }

    if (data.confirmPassword !== data.password) {
      setError({
        ...signUpErrors,
        confirmPassword: "Confirm password is not the same as password"
      });
      return false;
    }

    if (data.firstName.length < 3) {
      setError({
        ...signUpErrors,
        firstName: "You need at least 3 characters"
      });
      return false;
    }

    if (data.lastName.length < 3) {
      setError({
        ...signUpErrors,
        lastName: "You need at least 3 characters"
      });
      return false;
    }

    if (data.mobile.length !== 11) {
      setError({
        ...signUpErrors,
        mobile: "Mobile not valid"
      });
      return false;
    }

    return true;
  };

  const handleSubmit = event => {
    //const isLoading = !toggleLoading;
    event.preventDefault();
    const isValid = validate(signUpDetails);
    if (isValid) {
      signUpPost();
    }
    setSignUpDetails({
      email: "",
      password: "",
      confirmPassword: "",
      firstName: "",
      lastName: "",
      mobile: ""
    });
  };

  const handleChange = event => {
    const { name, value } = event.target;
    setSignUpDetails({
      ...signUpDetails,
      [name]: value
    });
  };

  const backToLogin = () => {
    const loginScreen = true;
    dispatch({
      type: setLogin,
      payload: loginScreen
    });
  };

  return (
    <div>
      <form className="sign-up" onSubmit={event => handleSubmit(event)}>
        <div className="s">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <input
            type="email"
            onChange={handleChange}
            className="form-control"
            id="email"
            value={signUpDetails.email}
            aria-describedby="emailHelp"
            placeholder="Enter email"
            name="email"
          />
          {signUpErrors.email ? (
            <small style={{ color: "red" }}>{signUpErrors.email}</small>
          ) : null}
          <br />
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            value={signUpDetails.password}
            onChange={handleChange}
            className="form-control"
            id="password"
            aria-describedby="passwordHelp"
            placeholder="Enter password"
          />
          {signUpErrors.password ? (
            <small style={{ color: "red" }}>{signUpErrors.password}</small>
          ) : null}
        </div>
        <br />
        <div>
          <label htmlFor="confirm-password">Confirm Password</label>
          <input
            value={signUpDetails.confirmPassword}
            type="password"
            name="confirmPassword"
            value={signUpDetails.confirmPassword}
            onChange={handleChange}
            className="form-control"
            id="confirm-password"
            aria-describedby="passwordHelp"
            placeholder="Confirm password"
          />
          {signUpErrors.confirmPassword ? (
            <small style={{ color: "red" }}>
              {signUpErrors.confirmPassword}
            </small>
          ) : null}
        </div>
        <p />
        <div>
          <label htmlFor="firstName">firstName</label>
          <input
            value={signUpDetails.firstName}
            type="firstName"
            name="firstName"
            value={signUpDetails.firstName}
            onChange={handleChange}
            className="form-control"
            id="login-firstName"
            aria-describedby="firstNameHelp"
            placeholder="Enter firstName"
          />
          {signUpErrors.firstName ? (
            <small style={{ color: "red" }}>{signUpErrors.firstName}</small>
          ) : null}
        </div>
        <p />
        <div>
          <label htmlFor="lastName">lastName</label>
          <input
            value={signUpDetails.lastName}
            type="lastName"
            name="lastName"
            value={signUpDetails.lastName}
            onChange={handleChange}
            className="form-control"
            id="login-lastName"
            aria-describedby="lastNameHelp"
            placeholder="Enter lastName"
          />
          {signUpErrors.lastName ? (
            <small style={{ color: "red" }}>{signUpErrors.lastName}</small>
          ) : null}
        </div>
        <p />
        <div>
          <label htmlFor="mobile">Mobile</label>
          <input
            value={signUpDetails.mobile}
            type="mobile"
            name="mobile"
            value={signUpDetails.mobile}
            onChange={handleChange}
            className="form-control"
            id="login-mobile"
            aria-describedby="mobileHelp"
            placeholder="Enter mobile"
          />
          {signUpErrors.mobile ? (
            <small style={{ color: "red" }}>{signUpErrors.mobile}</small>
          ) : null}
        </div>
        <p />
        <button type="submit" className="btn btn-primary">
          Sign Up
        </button>
        <br />
        <small>
          Already have an account ? login{" "}
          <Link to="/" onClick={backToLogin}>
            here
          </Link>
        </small>

        {superrors ? <div>{superrors.email}</div> : <div>no errors</div>}

        {/* <button className="btn btn-primary" onClick={() => { alert("BANANA")}}>LAILA</button> */}
      </form>
    </div>
  );
};

export default SignUp;
