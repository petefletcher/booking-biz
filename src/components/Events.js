const now = new Date()


export default [
  {
    id: 0,
    title: 'Cover Up',
    start: new Date(2019, 3, 2, 9, 0, 0),
    end: new Date(2019, 3, 2, 12, 0, 0),
    resourceId: 1,
  },
  {
    id: 1,
    title: 'Japanese Dragons',
    start: new Date(2019, 3, 2, 14, 0, 0),
    end: new Date(2019, 3, 2, 16, 20, 0),
    resourceId: 2,
  },
  {
    id: 2,
    title: 'DotWork of Sauron',
    start: new Date(2019, 3, 2, 8, 30, 0),
    end: new Date(2019, 3, 2, 12, 30, 0),
    resourceId: 3,
  },
  {
    id: 11,
    title: 'Back And Grey Portriat',
    start: new Date(2019, 3, 2, 7, 0, 0),
    end: new Date(2019, 3, 2, 10, 2, 0),
    resourceId: 4,
  },
  {
    id: 1,
    title: 'ButterFly Piece',
    start: new Date(2019, 3, 2, 14, 0, 0),
    end: new Date(2019, 3, 2, 16, 20, 0),
    resourceId: 5,
  },
  {
    id: 2,
    title: 'ForeArm laser',
    start: new Date(2019, 3, 2, 8, 30, 0),
    end: new Date(2019, 3, 2, 12, 30, 0),
    resourceId: 6,
  },
  {
    id: 11,
    title: 'Tongue Piercing',
    start: new Date(2019, 3, 2, 7, 0, 0),
    end: new Date(2019, 3, 2, 10, 2, 0),
    resourceId: 7,
  },
]
