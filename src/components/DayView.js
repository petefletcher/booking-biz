import React, { useState, useEffect } from "react";
import times from "../utils/times.json";
import "./componentscss/DayView.css";
import AppointmentModal from "./AppointmentModal.js";
import EditEventModal from "./EditEventModal";
import { useSelector, useDispatch } from "react-redux";
import { saveApt, editApt, deleteApt } from "../store/actions";

const DayView = () => {
  const [appointmentModal, setAppointmentModal] = useState(false);
  const [editEventModal, setEditEventModal] = useState(false);
  const [editEvent, setEditEvent] = useState({});
  const [timeClicked, setTimeClicked] = useState("");
  const [employeeId, setEmployeeId] = useState("");
  // const [currentDay, setCurrentDay] = useState(dateFns.startOfToday())
  const [timeNumb, setTimeNumb] = useState("");

  const storeEmployees = useSelector(state => state.employeesdb.employees);
  const storeEvents = useSelector(state => state.eventsFile.events);
  const dayViewDate = useSelector(state => state.eventsFile.dayViewDate);
  const appointmentView = useSelector(
    state => state.eventsFile.viewAppointments
  );
  const dispatch = useDispatch();

  const [events, setEvents] = useState(storeEvents);

  useEffect(() => {
    const updateEvents = [];
    events.forEach(element => {
      updateEvents.push(element);
    });
    setEvents(updateEvents);
    return () => {
      console.log("Clean-up-events");
    };
  }, []);

  let aptModalClose = () => setAppointmentModal(false);
  let editModalClose = () => setEditEventModal(false);

  const individualEmployeeColumn = () => {
    let employeeCol = [];
    let employeeCells = [];

    employeeCol.push(
      <div
        className={`employees-column-${appointmentView}`}
        key={"key-" + [appointmentView]}
      >
        <div id={appointmentView}>{employeeCells}</div>
        <div className="eventsContainer">
          {renderEvents(`employees-column-${appointmentView}`)}
        </div>
      </div>
    );

    for (let i = 0; i < times.length; i++) {
      const cloneTime = times[i];
      const id = i;
      employeeCells.push(
        <div
          className="employee-cells"
          key={cloneTime}
          id={i}
          onClick={event => onDateClick(event, cloneTime, id)}
        />
      );
    }

    return employeeCol;
  };

  const employeeColumn = () => {
    let employeeCol = [];
    let employeeCells = [];

    for (let i = 0; i < storeEmployees.length; i++) {
      employeeCol.push(
        <div
          className={`employees-column-${storeEmployees[i].name}-${[i]}`}
          key={"key-" + [i]}
        >
          <div id={storeEmployees[i].name}>{employeeCells}</div>
          <div className="eventsContainer">
            {renderEvents(`employees-column-${storeEmployees[i].name}`)}
          </div>
        </div>
      );
    }

    for (let i = 0; i < times.length; i++) {
      const cloneTime = times[i];
      const id = i;
      employeeCells.push(
        <div
          className="employee-cells"
          key={cloneTime}
          id={i}
          onClick={event => onDateClick(event, cloneTime, id)}
        />
      );
    }

    return employeeCol;
  };

  const renderEvents = classname => {
    let eventsArr = [...events];

    let employeeEvents = [];

    const employee = classname.slice(17);
    eventsArr.forEach(element => {
      if (
        element.date.toString().slice(0, 15) ===
        dayViewDate.toString().slice(0, 15)
      ) {
        let slot = element.timeId;
        let start = slot * 50;
        let slotHeight = element.finishTimeId - parseFloat(slot);
        let finishtime = slotHeight * 50;
        if (employee === element.employeeId) {
          employeeEvents.push(
            <div
              className="event"
              key={"key-" + element.client}
              style={{ top: start + "px", height: finishtime + "px" }}
              onClick={() => handleClickedEvent(element)}
            >
              {element.client}
              <div>
                {element.startTime} - {element.endTime}
              </div>
            </div>
          );
        }
      }
    });
    return employeeEvents;
  };

  const handleClickedEvent = element => {
    const editElement = element;
    const doesShow = editEventModal;
    setEditEventModal(!doesShow);
    setAppointmentModal(doesShow);
    setEditEvent(editElement);
  };

  const addDayTimes = () => {
    let aptTimes = [];

    for (let i = 0; i < times.length; i++) {
      aptTimes.push(
        <div className="hour-times" key={"key-" + [i]} id={i}>
          {times[i]}
        </div>
      );
    }
    return <div className="hours">{aptTimes}</div>;
  };

  const renderDayView = () => {
    let hourWrap = [];
    let times = addDayTimes();
    let employees = {};

    if (appointmentView === "All") {
      let employeesColumn = employeeColumn();
      employees = employeesColumn;
    } else {
      let employeeColumn = individualEmployeeColumn();
      employees = employeeColumn;
    }

    hourWrap.push(
      <div className="hour-wrapper" key={"key-hour-wrap"}>
        {times}
        {employees}
      </div>
    );

    return <div className="hours-wrapper">{hourWrap}</div>;
  };

  const renderEmployees = () => {
    const employees = [];
    const employeeArray = storeEmployees;

    if (appointmentView === "All") {
      for (let i = 0; i < employeeArray.length; i++) {
        // if the creen size is less than 411
        if (window.screen.width <= 410) {
          employees.push(
            <div className="employee-names" key={i}>
              {employeeArray[i].name.charAt(0)}
            </div>
          );
        } else {
          employees.push(
            <div className="employee-names" key={i}>
              {employeeArray[i].name}
            </div>
          );
        }
      }
    } else {
      employees.push(
        <div className="employee-names" key={appointmentView}>
          {appointmentView}
        </div>
      );
    }

    return <div className="employee-names-wrapper">{employees}</div>;
  };

  const saveAppointment = appointmentData => {
    setEvents([...events, appointmentData]);
    dispatch({
      type: saveApt,
      payload: [...events, appointmentData]
    });
  };

  const saveEditedAppointment = editedData => {
    const eventArr = [...events];
    eventArr.forEach((element, index) => {
      if (
        element.date === editedData.date &&
        element.employeeId === editedData.employeeId
      ) {
        eventArr.splice(index, 1, editedData);
      }
    });
    setEvents(eventArr);
    dispatch({
      type: editApt,
      payload: eventArr
    });
  };

  const deleteAppointment = editedData => {
    const eventArr = [...events];
    eventArr.forEach((element, index) => {
      if (
        element.date === editedData.date &&
        element.employeeId === editedData.employeeId
      ) {
        eventArr.splice(index, 1);
      }
    });
    setEvents(eventArr);
    dispatch({
      type: deleteApt,
      payload: eventArr
    });
  };

  const onDateClick = (event, time, timeid) => {
    const newTimeId = timeid.toString();
    const id = event.target.parentNode.id;
    const doesShow = appointmentModal;
    setEditEventModal(doesShow);
    setEmployeeId(id);
    setTimeClicked(time);
    setTimeNumb(newTimeId);
    setAppointmentModal(!doesShow);
  };
  return (
    <div>
      <div className="name-wrapper">
        <div className="employee-namespacer" />
        {renderEmployees()}
      </div>
      {renderDayView()}

      {appointmentModal ? (
        <AppointmentModal
          time={timeClicked}
          day={dayViewDate}
          onHide={aptModalClose}
          id={employeeId}
          saveAppointment={saveAppointment}
          newTimeId={timeNumb}
        />
      ) : null}
      {editEventModal ? (
        <EditEventModal
          onHide={editModalClose}
          event={editEvent}
          saveEditedAppointment={saveEditedAppointment}
          deleteAppointment={deleteAppointment}
        />
      ) : null}
    </div>
  );
};

export default DayView;
