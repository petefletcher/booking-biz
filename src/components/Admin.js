import React, { useState } from "react";
import "./componentscss/Admin.css";

//make auth and firstore references

const Admin = props => {
  const [email, setemail] = useState({ email: "" });
  //add admin cloud function
  const handleSubmit = event => {
    event.preventDefault();
    // const adminEmail = email;
    // addAdminRole({email: adminEmail.email}).then(result => {
    //   console.log('This is RESULT',result)
    // }).catch (err => {
    //   return console.log(err);
    // }) // return function

    // console.log('LAST EMAIL DEBUG ',adminEmail);
  };

  const handleChange = event => {
    const adminEmail = event.target.value;
    setemail({ email: adminEmail });
  };

  return (
    <form className="add-admin" onSubmit={event => handleSubmit(event)}>
      <div className="s">
        <label htmlFor="exampleInputEmail1">Email address</label>
        <input
          type="email"
          onChange={e => {
            handleChange(e);
          }}
          className="form-control"
          id="exampleInputEmail1"
          aria-describedby="emailHelp"
          placeholder="Enter email"
        />
      </div>
      <button type="submit" className="btn btn-primary">
        Submit
      </button>
    </form>
  );
};

export default Admin;
