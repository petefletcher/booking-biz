import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { navigate } from "@reach/router";
import { useState } from "react";
import axios from "axios";
import { setErrors, clearErrors, loadingUi, setUser } from "../store/actions";

const Login = props => {
  const { auth, children } = props;
  const loginErrors = useSelector(state => state.uiFunctionality.errors);
  const [loginDetails, setLoginDetails] = useState({ email: "", password: "" });
  const dispatch = useDispatch();

  const getUserData = () => {
    axios
      .get("http://localhost:5000/api/user")
      .then(res => {
        dispatch({ type: setUser, payload: res.data.userInfo });
        dispatch({ type: clearErrors });
        navigate(`/dashboard`);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const loginUser = userData => {
    dispatch({
      type: loadingUi,
      payload: true
    });

    axios
      .post(`http://localhost:5000/api/login`, userData)
      .then(result => {
        const firebaseToken = `Bearer ${result.data.token}`;
        localStorage.setItem("firebaseToken", firebaseToken);
        /**axios command below allows us to access the different claims in the token */
        axios.defaults.headers.common["Authorization"] = firebaseToken;
        getUserData();
        dispatch({
          type: loadingUi,
          payload: false
        });
      })
      .catch(err => {
        dispatch({
          type: setErrors,
          payload: err.response.data
        });
      });
  };

  const handleSubmit = event => {
    event.preventDefault();
    loginUser(loginDetails);
  };

  const handleChange = event => {
    const { name, value } = event.target;
    setLoginDetails({
      ...loginDetails,
      [name]: value
    });
  };
  /**We pass in authenticated and see if its true, if it is true then
   * we render the children within login
   * else we render the login form
   */
  return auth ? (
    children
  ) : (
    <div>
      <form className="add-admin" onSubmit={event => handleSubmit(event)}>
        <div className="s">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <input
            type="email"
            onChange={handleChange}
            className="form-control"
            id="login-email"
            value={loginDetails.email}
            aria-describedby="emailHelp"
            placeholder="Enter email"
            name="email"
          />
          <p />
          <label htmlFor="password">Password</label>
          <input
            value={loginDetails.password}
            type="password"
            name="password"
            onChange={handleChange}
            className="form-control"
            id="login-password"
            aria-describedby="passwordHelp"
            placeholder="Enter password"
          />
        </div>
        <p />
        <button
          type="submit"
          className="btn btn-primary"
          disabled={!loginDetails.email || !loginDetails.password}
        >
          Login
        </button>
        <br />
        {loginErrors ? <div>{loginErrors.error}</div> : null}

        {/* <button className="btn btn-primary" onClick={() => { alert("BANANA")}}>LAILA</button> */}
      </form>
    </div>
  );
};

export default Login;
