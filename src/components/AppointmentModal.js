import React from "react";
import times from "../utils/times.json";
import "./componentscss/AppointmentModal.css";

class AppointmentModal extends React.Component {
  state = {
    appointmentData: {
      date: this.props.day.toString(),
      employeeId: this.props.id,
      startTime: this.props.time,
      endTime: this.props.time,
      client: "",
      details: "",
      timeId: this.props.newTimeId
    }
  };
  render() {
    const day = this.props.day.toString().slice(0, 15);
    const id = this.props.id;

    return (
      <div className="appointment-modal-content">
        <div className="appointment-modal-header">
          <h5 className="appointment-modal-title" id="exampleModalLabel">
            Appointment
          </h5>
        </div>
        <div className="appointment-modal-body">
          <form id="appointmentData" onSubmit={this.handleSubmit}>
            <div className="appointment-form-group">
              <label>Date</label>
              <input
                type="text"
                className="appointment-form-control"
                id="date"
                value={day}
                disabled
              />
            </div>
            <div className="appointment-form-group">
              <label>Booking With</label>
              <input
                type="text"
                className="appointment-form-control"
                id="date"
                value={id}
                disabled
              />
            </div>
            <div className="appointment-form-group">
              <label>Start Time</label>
              <select
                className="appointment-form-control"
                id="startTime"
                onChange={this.selectOnChange}
                value={this.state.appointmentData.startTime}
              >
                {this.renderStartTimes()}
              </select>
            </div>
            <div className="appointment-form-group">
              <label>End Time</label>
              <select
                className="appointment-form-control"
                id="endTime"
                onChange={this.selectOnChange}
                value={this.state.appointmentData.endTime}
              >
                {this.renderFinishTimes()}
              </select>
            </div>
          </form>
          <div className="appointment-form-group">
            <label>Client Name</label>
            <input
              type="text"
              className="appointment-form-control"
              id="client"
              onChange={this.selectOnChange}
              value={this.state.appointmentData.client}
            />
          </div>
          <div className="appointment-form-group">
            <label>Details</label>
            <textarea
              className="appointment-form-control"
              id="details"
              rows="3"
              onChange={this.selectOnChange}
              value={this.state.appointmentData.details}
            />
          </div>
        </div>
        <div className="appointment-modal-footer">
          <button
            type="button"
            className="close-button"
            data-dismiss="modal"
            onClick={this.props.onHide}
          >
            Close
          </button>
          <button
            type="button"
            className="save-button"
            onClick={event => {
              this.selectOnChange(event);
              this.props.onHide();
              this.props.saveAppointment(this.state.appointmentData);
            }}
          >
            Save changes
          </button>
        </div>
      </div>
    );
  }
  renderStartTimes = function() {
    let aptTimes = [];

    for (let i = 0; i < times.length; i++) {
      aptTimes.push(
        <option
          value={times[i]}
          key={"start-times-key-" + times[i]}
          id={"startTimeId" + i}
        >
          {times[i]}
        </option>
      );
    }

    return aptTimes;
  };
  renderFinishTimes = () => {
    let aptTimes = [];

    for (let i = 0; i < times.length; i++) {
      aptTimes.push(
        <option
          value={times[i]}
          key={"start-times-key-" + times[i]}
          id={"finishTimeId" + i}
        >
          {times[i]}
        </option>
      );
    }
    return aptTimes;
  };

  selectOnChange = event => {
    const { id, value } = event.target;
    let finishId = this.state.appointmentData.finishTimeId;
    if (id === "endTime") {
      finishId = event.target.selectedIndex;
    }
    // const option = optionElement.getSelectedAttribute('data-id')
    this.setState({
      appointmentData: {
        ...this.state.appointmentData,
        finishTimeId: finishId,
        [id]: value
      }
    });
  };
}

export default AppointmentModal;
