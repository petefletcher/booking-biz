import React from "react";
import '../componentscss/Dashboard.css'


class AppointmentsCard extends React.Component {
  state = {};
  render() {
    return (
      <section className="infoCards">
       <div className="infocard-header">Total Appointments
       </div>
       <div className="infocard-row" ><div className="info-card-text">Todays Appointments</div><div className="card-number">10</div></div>
       <div className="infocard-row"><div className="info-card-text">Appointments In Next 7 Days</div><div className="card-number">10</div>
       </div>
       <div className="infocard-row"><div className="info-card-text">Appointments In Next 30 Days</div><div className="card-number">10</div>
       </div>
      </section>
    );
  }
}

export default AppointmentsCard;