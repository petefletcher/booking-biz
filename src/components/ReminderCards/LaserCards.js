import React from "react";
import '../componentscss/Dashboard.css'


class LaserCard extends React.Component {
  state = {};
  render() {
    return (
      <section className="infoCards">
       <div className="infocard-header">Total Laser Treatment
       </div>
       <div className="infocard-row" ><div className="info-card-text">Todays Lasers</div><div className="card-number">10</div>
       </div>
       <div className="infocard-row"><div className="info-card-text">Lasers In Next 7 Days</div><div className="card-number">10</div>
       </div>
       <div className="infocard-row"><div className="info-card-text">Lasers In Next 30 Days</div><div className="card-number">10</div>
       </div>
      </section>
    );
  }
}

export default LaserCard;