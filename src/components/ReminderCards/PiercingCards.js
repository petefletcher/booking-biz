import React from "react";
import '../componentscss/Dashboard.css'


class PiercingCard extends React.Component {
  state = {};
  render() {
    return (
      <section className="infoCards">
       <div className="infocard-header">Total Piercings
       </div>
       <div className="infocard-row" ><div className="info-card-text">Piercings Today</div><div className="card-number">10</div>
       </div>
       <div className="infocard-row"><div className="info-card-text">Piercings In Next 7 Days</div><div className="card-number">10</div>
       </div>
       <div className="infocard-row"><div className="info-card-text">Piercings In Next 30 Days</div><div className="card-number">10</div>
       </div>
      </section>
    );
  }
}

export default PiercingCard;