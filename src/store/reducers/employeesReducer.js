import { employeesFile, employeesData } from "../../utils/employees";
//import {saveApt, editApt, deleteApt} from '../store/actions';

const initialState = {
  employees: employeesFile,
  data: employeesData
};

console.log("Employees Reducer", initialState);

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "a":
      return {
        ...state,
        events: action.payload
      };
    case "b":
      return {
        ...state,
        events: action.payload
      };
    case "c":
      return {
        ...state,
        events: action.payload
      };
    //need a different reducer here
    default:
      return state;
  }
};

export default reducer;
