import {setErrors, setUser, clearErrors, loadingUi, setAuthenticated, setUnauthenticated } from '../actions';

const initialState = {
    authenticated: true,
    credentials: {}
}

console.log("Users Reducer", initialState)

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case setAuthenticated:
        return {
            ...state,
            authenticated: true
        }
        case setUser:
        return {
            authenticated: true,
            credentials: action.payload
        }
        default:
        return state;
    }
};

export default reducer;