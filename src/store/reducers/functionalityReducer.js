import { loadingUi, toggleSidebar, clearErrors, setErrors, setLogin } from "../actions";

const initialState = {
  sideMenuToggle: false,
  loading: false,
  errors: null,
  login: true,
  signup: false
};

console.log("uiReducer", initialState);

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case toggleSidebar:
      return {
        ...state,
        sideMenuToggle: action.payload
      };
    case loadingUi:
      return {
        ...state,
        loading: action.payload
      };
    case setErrors:
      return {
        ...state,
        loading: false,
        errors: action.payload
      };
    case clearErrors:
      return {
        ...state,
        loading: false,
        errors: null
      };
    case setLogin:
      return {
        ...state,
        login: action.payload,
        signup: !action.payload
      };
    default:
      return state;
  }
};

export default reducer;
