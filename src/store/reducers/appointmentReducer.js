import events from "../../utils/appointments.json";
import { saveApt, editApt, deleteApt, dayHeader, updateView } from "../actions";
import dateFns from "date-fns";

const initialState = {
  events: events,
  sideMenuToggle: false,
  dayViewDate: dateFns.startOfToday(),
  viewAppointments: "All"
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case saveApt:
      return {
        ...state,
        events: action.payload
      };
    case editApt:
      return {
        ...state,
        events: action.payload
      };
    case deleteApt:
      return {
        ...state,
        events: action.payload
      };
    case dayHeader:
      return {
        ...state,
        dayViewDate: action.payload
      };
    case updateView:
      return {
        ...state,
        viewAppointments: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
