/** Events */
export const saveApt = 'save-appointment';
export const editApt = 'edit-appointment';
export const deleteApt = 'delete-appointment';
export const dayHeader = 'day-header';
export const updateView = "update-view";

/** User */
export const setUser = 'set-user';
export const setAuthenticated = 'set-authenticated';
export const signUpUser = 'sign-up-user';

/** UI */
export const clearErrors = 'clear-errors';
export const setErrors = 'set-errors';
export const loadingUi = 'toggle-loading';
export const toggleSidebar = 'toggle-sidebar';
export const setLogin = 'set-login';
export const setSignup = 'set-signup';

