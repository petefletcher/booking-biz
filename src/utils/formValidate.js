export const validate = data => {
    //.*matches any char
    //\d any digit
    //{4,15} four to 15 chars
  const passwordCheck = new RegExp("^(?=.*\d){4,15}$");

  if(data.email === '') {
      setError({
          ...errors,
          email: "Email must not be empty"
       });
       return false
  }

  if(data.password === '') {
      setError({
          ...errors,
          password: "Password must not be empty"
       });
       return false
  }

  if(data.confirmPassword === '') {
      setError({
          ...errors,
          confirmPassword: "Password must not be empty"
       });
       return false
  }

  if(data.firstName === '') {
      setError({
          ...errors,
          firstName: "firstName must not be empty"
       });
       return false
  }

  if(data.lastName === '') {
      setError({
          ...errors,
          lastName: "lastName must not be empty"
       });
  }

  if(data.mobile === '') {
      setError({
          ...errors,
          mobile: "Mobile must not be empty"
       });
       return false
  }

  if (!data.email.includes("@")) {
    setError({
      ...errors,
      email: "Email not valid"
    });
    return false;
  }

  if(passwordCheck.test(data.password)) {
      setError({
          ...errors,
          password: "Passwords must must be be 4 chars long and include at least one numeric digit"
        });
      return false
  }

  if(data.confirmPassword !== data.password) {
      setError({
          ...errors,
          confirmPassword: "Confirm password is not the same as password"
        });
      return false
  }

  if(data.firstName.length < 3) {
      setError({
          ...errors,
          firstName: "You need at least 3 characters"
        });
      return false
  }

  if(data.lastName.length < 3) {
      setError({
          ...errors,
          lastName: "You need at least 3 characters"
        });
      return false
  }

  if(data.mobile.length !== 11) {
      setError({
          ...errors,
          mobile: "Mobile not valid"
        });
      return false
  }


  return true;
};