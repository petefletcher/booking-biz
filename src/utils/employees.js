export const employeesFile = [
  { name: "Dan", service: "tattoo", email: "dan@dan.com" },
  { name: "Paul", service: "laser", email: "paul@paul.com" },
  { name: "Megan", service: ["tattoo", "piecings"], email: "megan@megan.com" },
  { name: "Jamie", service: "tattoo", email: "jamie@jamie.com" },
  { name: "Aiden", service: "tattoo", email: "aiden@aiden.com" },
  { name: "Jenny", service: "piercings", email: "piercings@piercings.com" }
];

export const employeesData = [
  {
    employeeFirstName: "Dan",
    employeeLastName: "Watson",
    service: "tattoo",
    email: "dan@dan.com",
    mobile: "123456789909"
  },
  {
    employeeFirstName: "Paul",
    employeeLastName: "Watson",
    service: "laser",
    email: "paul@paul.com",
    mobile: "123456789909"
  },
  {
    employeeFirstName: "Megan",
    employeeLastName: "Watson",
    service: ["tattoo", "piecings"],
    email: "megan@megan.com",
    mobile: "123456789909"
  },
  {
    employeeFirstName: "Jamie",
    employeeLastName: "Watson",
    service: "tattoo",
    email: "jamie@jamie.com",
    mobile: "123456789909"
  },
  {
    employeeFirstName: "Aiden",
    employeeLastName: "Watson",
    service: "tattoo",
    email: "aiden@aiden.com",
    mobile: "123456789909"
  },
  {
    employeeFirstName: "Jenny",
    employeeLastName: "Watson",
    service: "piercings",
    email: "piercings@piercings.com",
    mobile: "123456789909"
  }
];
