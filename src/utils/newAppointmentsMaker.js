const dateFns = require("date-fns");
const fs = require("fs");
const users = require("./users.json");
const times = require("./times.json");

const randomDate = () => {
  const employees = [
    { name: "Dan", service: "tattoo", email: "dan@dan.com" },
    { name: "Paul", service: "laser", email: "paul@paul.com" },
    {
      name: "Megan",
      service: ["tattoo", "piecings"],
      email: "megan@megan.com"
    },
    { name: "Jamie", service: "tattoo", email: "jamie@jamie.com" },
    { name: "Aiden", service: "tattoo", email: "aiden@aiden.com" },
    { name: "Jenny", service: "piercings", email: "piercings@piercings.com" }
  ];

  const max = 12;
  let timeId = 1;
  let finishTimeId = 1;
  let min = 6;

  const events = [];
  let usersArr = [...users];
  const timesArr = [...times];

  /**Get random employee */
  for (let i = 0; i < 50; i++) {
    let event = {};
    let randEmployee = employees[Math.floor(Math.random() * employees.length)];

    //give a random date
    let randNumb = Math.floor(Math.random() * Math.floor(max));

    let randminNumb = Math.floor(Math.random() * (max - min + 1) + min);
    let date = dateFns.startOfDay(new Date());
    date = dateFns.addDays(new Date(2020, 0, randNumb), randNumb);
    date = dateFns.format(date, "ddd MMM DD YYYY");

    /**random user */
    let randUserNumb = Math.floor(Math.random() * Math.floor(usersArr.length));

    /**Random the start and finish times */
    timeId = Math.floor(Math.random() * Math.floor(max));
    while (finishTimeId < timeId) {
      finishTimeId = Math.floor(Math.random() * Math.floor(max));
    }

    event.client = `${usersArr[randUserNumb].firstName} ${usersArr[randUserNumb].lastName}`;
    event.employeeId = randEmployee.name;
    event.timeId = timeId;
    event.startTime = timesArr[timeId];
    event.endTime = timesArr[finishTimeId];
    event.finishTimeId = finishTimeId;
    event.date = date;
    events.push(event);
  }

  fs.writeFile(
    "./src/utils/appointments.json",
    JSON.stringify(events),
    function(err) {
      if (err) {
        return console.log(err);
      }
      console.log("The appointment file was saved!");
    }
  );
};

randomDate();
