var faker = require("faker");
var fs = require("fs");

const addUsers = () => {
  const userArr = [];

  for (let i = 0; i < 50; i++) {
    let user = {};
    user.firstName = faker.name.firstName();
    user.lastName = faker.name.lastName();
    user.email = `${user.firstName}.${user.lastName}@mail.com`;
    user.phoneNumber = faker.phone.phoneNumber();
    userArr.push(user);
  }

  fs.writeFile("./src/utils/users.json", JSON.stringify(userArr), function(
    err
  ) {
    if (err) {
      return console.log(err);
    }
    console.log("The file was saved!");
  });
};

addUsers();
