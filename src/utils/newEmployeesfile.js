function employeesFile () {
    return [
        {name: 'Dan', service: 'tattoo', email: 'dan@dan.com'},
        {name: 'Paul', service: 'laser', email: 'paul@paul.com'},
        {name: 'Megan', service: ['tattoo', 'piecings'], email: 'megan@megan.com'},
        {name: 'Jamie', service: 'tattoo', email: 'jamie@jamie.com'},
        {name: 'Aiden', service: 'tattoo', email: 'aiden@aiden.com'},
        {name: 'Jenny', service: 'piercings', email: 'piercings@piercings.com'}
    ]
} 

export default employeesFile;