import axios from "axios";

const BASE_URL = "http://localhost:5000/api"

/** Google Calendar */
export const authorizeCalendar = async () => {
  const {data} = await axios.get(`${BASE_URL}/authorizeCalendar`)
  console.log(`${BASE_URL}/authorizeCalendar`);
  return data
};

/** get code for auth */
export const postAuthCalCode = async (code) => {
  const {data} = await axios.post(`${BASE_URL}/api/authCode`, code)
  return data
};


/** User */
export const getUserData = async () => {
  const {data} = await axios.get(`${BASE_URL}/user`)
  return data
};

export const updateUserImage = async (image) => {
  const {data} = await axios.post(`${BASE_URL}/user/image`, image)
  return data
}

/** Employees */
export const getEmployeeData = async (query) => {
  const {data} = await axios.get(`${BASE_URL}/employee/?id=${query}`)
  return data
};

export const getAllEmployees = async () => {
  const {data} = await axios.get(`${BASE_URL}/getemployees`)
  return data;
}

export const editEmployee = async (employeeData) => {
  const {data} = await axios.put(`${BASE_URL}/editEmployee`, employeeData)
  return data;
}

export const removeAsEmployee = async (empEmail) => {
  const {data} = await axios.delete(`${BASE_URL}/removeEmployee/?id=${empEmail}`)
  return data
};

export const addUserAsEmployee = async (newEmployee) => {
  const {data} = await axios.post(`${BASE_URL}/createemployee/`, newEmployee)
  return data
};

export const addEmployeeAsAdmin = async (newAdmin) => {
  const {data} = await axios.post(`${BASE_URL}/setAdminRole/`, newAdmin)
  return data
};
