import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore, combineReducers, compose  } from 'redux';
import { Provider} from 'react-redux'
import functionalityReducer from './store/reducers/functionalityReducer';
import appointmentReducer from './store/reducers/appointmentReducer';
import employeesReducer from './store/reducers/employeesReducer';
import usersReducer from './store/reducers/userReducers';

const rootReducer = combineReducers({
    uiFunctionality: functionalityReducer,
    employeesdb: employeesReducer,
    eventsFile: appointmentReducer,
    usersdb: usersReducer
})

const store = createStore(rootReducer, compose(window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));


ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
