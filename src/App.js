import React from "react";
import { Router } from "@reach/router";
import { useSelector } from "react-redux";
// import SignUpLine from "./components/SignUpLine";
// import Login from "./components/Login.js";
import Appointments from "./components/Appointments.js";
import Artists from "./components/Artists.js";
import Clients from "./components/Clients.js";
import Dashboard from "./components/Dashboard.js";
import Navbar from "./components/Navbar.js";
import Sidebar from "./components/SideBar.js";
import Admin from "./components/Admin.js";
import ArtistPage from "./components/ArtistPage";
import Footer from "./components/Footer.js";
// import Signup from "./components/Signup";
// import jwtDecode from "jwt-decode";
// import { getUserData } from "./utils/axiosCalls";
// import axios from "axios";
import "./App.css";

const App = () => {
  let userAuthenticated = useSelector(state => state.usersdb.authenticated);
  let userLogin = useSelector(state => state.uiFunctionality.login);
  userLogin = true;

  /**get the token --> see if it exists in the local storage
   * decode token
   * check if expired (exp) with data.now
   *  */
  // const token = localStorage.firebaseToken;
  // if(token) {
  //   const decodedToken = jwtDecode(token)
  //   console.log(decodedToken)
  //   if(decodedToken.exp * 1000 < Date.now()){
  //     navigate('/');
  //     userAuthenticated = false;
  //   } else {
  //     axios.defaults.headers.common['Authorization'] = token;
  //     userAuthenticated = true;
  //   }
  // }

  const sideMenuToggle = useSelector(
    state => state.uiFunctionality.sideMenuToggle
  );

  //console.log(userLogin);
  return (
    <div className="App">
      <Navbar />
      {sideMenuToggle ? <Sidebar /> : null}

      {/* {userLogin ? <Login path="/" auth={userAuthenticated}> */}
      <Router primary={false}>
        <Dashboard path="/dashboard" />
        <Appointments path="/appointments" />
        <Artists path="/artists" />
        <ArtistPage path="/artists/:artist-email" />
        <Clients path="/clients" />
        <Admin path="/admin" />
      </Router>
      {/* </Login> : <Signup path="/signup"/>}
      {userAuthenticated === false ? <SignUpLine/> : null} */}
      <Footer />
    </div>
  );
};

export default App;
